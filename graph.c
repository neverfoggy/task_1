#include <stdio.h>
#define N 2000

static void plot(int *a, size_t len)
{
    fprintf(stdout,
        "set datafile separator ','\n"
        //"set terminal png size 1024,768\n"
        "set title '%s'\n"
        "set xlabel '%s'\n"
        "set ylabel '%s'\n"
        "set key left top\n"
        "set grid\n",
        "Fly straight, damn it!", "x", "y"
    );
    fprintf(stdout, "plot '-' with points title 'samples' pt 7\n");
    for (size_t i = 0; i < len; i++) {
        fprintf(stdout, "%zu,%d\n", i, a[i]);
    }
}


static int HCF(int a, int b)
{
    int hcf = 1;
    for (int i = 1;i <= a; i++)
        if (a%i == 0 && b%i == 0)
            hcf = i;

    return hcf;
}

static int GCD(int a, int b)
{
    while (a!=b){
        if (a > b)
            a -= b;
        else
            b -= a;
    }
    return a;
}

int main()
{
    int a[N]={0};

    a[0] = 1;
    a[1] = 1;

    int prev = a[1];

    for (int i = 2;i <= N; i++) {
        if (HCF(prev,i) == 1)
            a[i] = prev + i + 1;        
        else
            a[i] = prev/GCD(prev,i);

        prev = a[i];
    }

    plot(a,N);

    return 0;
}
